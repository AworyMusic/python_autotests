import requests
import pytest

URL =  'https://api.pokemonbattle.me/v2'
TOKEN = '11d52f6e4fca3214c4b0150942f7772b'
HEADERS = {'Content-Type' : 'application/json', 
           'trainer_token' : TOKEN}

def test_status_code():
    response = requests.get(url = f'{URL}/trainers', params = {"trainer_id" : 292})
    assert response.status_code == 200

def test_part_of_response():
    response = requests.get(url = f'{URL}/trainers', params = {"trainer_id" : 292})
    assert response.json()['data'][0]['id'] == '292'
    assert response.json()['data'][0]['trainer_name'] == 'Дудон'
