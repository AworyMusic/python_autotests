import requests

URL =  'https://api.pokemonbattle.me/v2'
TOKEN = '11d52f6e4fca3214c4b0150942f7772b'
HEADERS = {'Content-Type' : 'application/json', 
           'trainer_token' : TOKEN}

body = {
    "name": "Бульбазавр",
    "photo": "https://dolnikov.ru/pokemons/albums/001.png"
}

response = requests.post(url = f'{URL}/pokemons', headers = HEADERS, json = body)

print(response.text)

data = response.json()
ID = data['id']

body = {
    "pokemon_id": ID,
    "name": "New Name",
    "photo": "https://dolnikov.ru/pokemons/albums/009.png"
}

response = requests.put(url = f'{URL}/pokemons', headers = HEADERS, json = body)

print(response.text)

body = {
    "pokemon_id": ID
}

response = requests.post(url = f'{URL}/trainers/add_pokeball', headers = HEADERS, json = body)

print(response.text)